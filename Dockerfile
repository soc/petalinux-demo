FROM gitlab-registry.cern.ch/mawyzlin/petalinux-docker/petalinux-v2020.2

RUN mkdir /home/vivado/petalinux-demo
COPY . /home/vivado/petalinux-demo

WORKDIR /home/vivado/petalinux-demo
RUN source /opt/Xilinx/petalinux/settings.sh && petalinux-build
