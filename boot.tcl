
connect
targets -set -nocase -filter {name =~ "*PS TAP*"}
rst -srst
after 1000
targets -set -nocase -filter {name =~ "*PSU*"}
mask_write 0xFFCA0038 0x1C0 0x1C0
targets -set -nocase -filter {name =~ "*MicroBlaze PMU*"}
puts stderr "INFO: Downloading ELF file: /home/soc-workshop/linux/pmufw.elf to the target."
dow  "/home/soc-workshop/linux/pmufw.elf"
after 2000
con
targets -set -nocase -filter {name =~ "*APU*"}
mwr 0xffff0000 0x14000000
mask_write 0xFD1A0104 0x501 0x0
after 5000
targets -set -nocase -filter {name =~ "*A53*#0"}

source /home/soc-workshop/linux/psu_init.tcl
puts stderr "INFO: Downloading ELF file: /home/soc-workshop/linux/zynqmp_fsbl.elf to the target."
dow  "/home/soc-workshop/linux/zynqmp_fsbl.elf"
after 2000
con
after 4000; stop; catch {stop}; psu_ps_pl_isolation_removal; psu_ps_pl_reset_config
targets -set -nocase -filter {name =~ "*A53*#0"}
puts stderr "INFO: Loading image: /home/soc-workshop/linux/system.dtb at 0x00100000"
dow -data  "/home/soc-workshop/linux/system.dtb" 0x00100000
after 2000
targets -set -nocase -filter {name =~ "*A53*#0"}
puts stderr "INFO: Downloading ELF file: /home/soc-workshop/linux/u-boot.elf to the target."
dow  "/home/soc-workshop/linux/u-boot.elf"
after 2000
targets -set -nocase -filter {name =~ "*A53*#0"}
puts stderr "INFO: Downloading ELF file: /home/soc-workshop/linux/bl31.elf to the target."
dow  "/home/soc-workshop/linux/bl31.elf"
after 2000
con
exit
puts stderr "INFO: Saving XSDB commands to boot.tcl. You can run 'xsdb boot.tcl' to execute"
